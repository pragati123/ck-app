package com.example.poko.someloginpage;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Poko on 08-08-2016.
 */
public interface CodekampApiServices {

    @GET("authenticate")
    Call<LoginAuthentication> checkAuthentication(@Query("email") String emailkey, @Query("password") String passwordkey);
}
