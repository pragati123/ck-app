
package com.example.poko.someloginpage;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginAuthentication {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("user")
    @Expose
    private List<User> user = new ArrayList<User>();

    /**
     * 
     * @return
     *     The token
     */
    public String getToken() {
        return token;
    }

    /**
     * 
     * @param token
     *     The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * 
     * @return
     *     The user
     */
    public List<User> getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(List<User> user) {
        this.user = user;
    }

}
